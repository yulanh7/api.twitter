<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TwitterTest extends TestCase
{
  protected $random;
  protected $user;

  /**
   * @return void
   */
  public function setUp(): void
  {
    parent::setUp();
    $this->random = \Str::random(10);
    $this->user = $this->getJson("api/v1/me/user", [])->decodeResponseJson();
  }

  /**
   * test getting tweets
   */
  public function test_get_tweets()
  {
    $response = $this->get('api/v1/tweets');
    $json = $response->decodeResponseJson();
    $this->assertGreaterThanOrEqual(0, $this->count($json));
    $response->assertStatus(200);
  }

  /**
   * test post and store a tweet
   */
  public function test_store_tweet()
  {
   
    $response = $this->postJson('api/v1/tweets', ['text' => 'unit test ' . $this->random]);
    $response->assertStatus(200);
    $response->assertJsonFragment([
      'text' => 'unit test ' . $this->random,
      'screen_name' => $this->user['screen_name'],
    ]);

    $response = $this->postJson('api/v1/tweets', ['text' => 'unit test ' . $this->random]);
    $response->assertStatus(500);
    $response->assertJsonFragment([
      'message' => 'Status is a duplicate.',
    ]);
  }

  public function test_reply_tweet()
  {
    $response = $this->postJson('api/v1/tweets', ['text' => 'unit test reply ' . $this->random]);
    $json = $response->decodeResponseJson();
    $response = $this->postJson('api/v1/tweets/replyTweet', [
      'text' => 'unit test reply ' . $this->random . ' test',
      'in_reply_to_status_id' => $json['tweet_id'],
    ]);
    $response->assertStatus(200);
    $response->assertJsonFragment([
      'text' => 'unit test reply ' . $this->random . ' test',
      'screen_name' => $this->user['screen_name'],
    ]);

    $response = $this->postJson('api/v1/tweets/replyTweet', [
      'text' => 'unit test reply ' . $this->random . ' test',
      'in_reply_to_status_id' => $json['tweet_id'],
      ]);
    $response->assertStatus(500);
    $response->assertJsonFragment([
      'message' => 'Status is a duplicate.',
    ]);
  }
}