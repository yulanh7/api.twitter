<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Tweet.
 *
 * @package namespace App\Entities;
 */
class Tweet extends Model implements Transformable
{
  use TransformableTrait;

  protected $table = 'tweets';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'tweet_id',
    'favorite_count',
    'retweet_count',
    'text',
    'user_id',
    'created_at',
  ];

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id', 'id');
  }
}
