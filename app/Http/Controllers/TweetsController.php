<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\TweetCreateRequest;
use App\Http\Requests\StarTweetRequest;
use App\Http\Requests\TweetUpdateRequest;
use App\Repositories\TweetRepository;
use App\Validators\TweetValidator;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Abraham\TwitterOAuth\TwitterOAuthException;
use App\Http\Requests\ReplyTweetRequest;

/**
 * Class TweetsController.
 *
 * @package namespace App\Http\Controllers;
 */
class TweetsController extends Controller
{
  /**
   * @var TweetRepository
   */
  protected $repository;
  protected $userRepository;
  protected $connection;
  protected $storeTweet;

  /**
   * @var TweetValidator
   */
  protected $validator;

  /**
   * TweetsController constructor.
   *
   * @param TweetRepository $repository
   * @param TweetValidator $validator
   */
  public function __construct(TweetRepository $repository, TweetValidator $validator, UserRepository $userRepository)
  {
    $this->repository = $repository;
    $this->validator  = $validator;
    $this->userRepository  = $userRepository;
    $CONSUMER_KEY = config('services.twitter.consumer_key');
    $CONSUMER_SECRET = config('services.twitter.consumer_secret');
    $access_token = config('services.twitter.access_token');
    $access_token_secret = config('services.twitter.access_token_secret');
    $this->connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token, $access_token_secret);
  }

  /**
   * Display a listing of tweets.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $results = $this->connection->get("statuses/home_timeline", ["count" => 25, "exclude_replies" => true]);
    $tweets = [];
    $users = [];
    $twitterLastHttpCode = $this->connection->getLastHttpCode();
    if ($twitterLastHttpCode == 200) {
      foreach ($results as $result) {
        $tweets[] = $this->storeTweet($result);

        $user = [];
        $user['id'] = $result->user->id;
        $user['name'] = $result->user->name;
        $user['screen_name'] = $result->user->screen_name;
        $user['location'] = $result->user->location;
        $user['description'] = $result->user->description;
        $user['followers_count'] = $result->user->followers_count;
        $user['friends_count'] = $result->user->friends_count;
        $user['listed_count'] = $result->user->listed_count;
        $user['created_at'] = Carbon::parse($result->user->created_at);
        $users[] = $user;
      }
      $this->userRepository->upsertMany($users);
      $this->repository->upsertMany($tweets);
      $response = $this->repository->with('user')->paginate();
    } else {
      return response()->json([
        'message' => $results->errors[0]->message
      ], 500);
    }
    return $response;
  }

  /**
   * Post a tweet and Store it in storage.
   *
   * @param  TweetCreateRequest $request
   *
   * @return \Illuminate\Http\Response
   *
   * @throws \Prettus\Validator\Exceptions\ValidatorException
   */
  public function store(TweetCreateRequest $request)
  {
    $result = $this->connection->post("statuses/update", ["status" => $request->text]);
    $response = [];
    $twitterLastHttpCode = $this->connection->getLastHttpCode();
    if ($twitterLastHttpCode == 200) {
      $tweets[] = $this->storeTweet($result);
      $this->repository->upsertMany($tweets);
      $response = $this->repository->with('user')->findWhere(['tweet_id' => $result->id]);
      if (count($response) > 0) {
        $response = $response[0];
      }
    } else {
      return response()->json([
        'message' => $result->errors[0]->message
      ], 500);
    }
    return $response;
  }

  /**
   * @param ReplyTweetRequest $request
   * 
   * @return [type]
   */
  public function replyTweet(ReplyTweetRequest $request)
  {
    $result = $this->connection->post("statuses/update", [
      "status" => $request->text,
      'in_reply_to_status_id' => $request->in_reply_to_status_id,
      'auto_populate_reply_metadata' => true,
    ]);
    $response = [];
    $twitterLastHttpCode = $this->connection->getLastHttpCode();
    if ($twitterLastHttpCode == 200) {
      $tweets[] = $this->storeTweet($result);
      $this->repository->upsertMany($tweets);
      $response = $this->repository->with('user')->findWhere(['tweet_id' => $result->id]);
      if (count($response) > 0) {
        $response = $response[0];
      }
    } else {
      return response()->json([
        'message' => $result->errors[0]->message
      ], 500);
    }
    return $response;
  }

  /**
   * @param mixed $id
   * 
   * @return [type]
   */
  public function retweet($id)
  {
    $result = $this->connection->post("statuses/retweet/" . $id);
    $response = [];
    $twitterLastHttpCode = $this->connection->getLastHttpCode();
    if ($twitterLastHttpCode == 200) {
      $tweets[] = $this->storeTweet($result);
      $tweet = $this->repository->upsertMany($tweets);
      $response = $this->repository->with('user')->findWhere(['tweet_id' => $result->id]);
      if (count($response) > 0) {
        $response = $response[0];
      }
    } else {
      return response()->json([
        'message' => $result->errors[0]->message
      ], 500);
    }
    return $response;
  }

  /**
   * @param StarTweetRequest $request
   * 
   * @return [type]
   */
  public function StarTweet(StarTweetRequest $request)
  {
    $result = $this->connection->post("favorites/create", ["id" => $request->id]);
    $response = [];
    $twitterLastHttpCode = $this->connection->getLastHttpCode();
    if ($twitterLastHttpCode == 200) {
      $tweets[] = $this->storeTweet($result);
      $tweet = $this->repository->upsertMany($tweets);
      $response = $this->repository->with('user')->findWhere(['tweet_id' => $result->id]);
      if (count($response) > 0) {
        $response = $response[0];
      }
    } else {
      return response()->json([
        'message' => $result->errors[0]->message
      ], 500);
    }
    return $response;
  }


  /**
   * @param mixed $result
   * 
   * @return [type]
   */
  public function storeTweet($result)
  {
    $tweet = [];
    $tweet['tweet_id'] = $result->id;
    $tweet['text'] = $result->text;
    $tweet['favorite_count'] = $result->favorite_count;
    $tweet['retweet_count'] = $result->retweet_count;
    $tweet['user_id'] = $result->user->id;
    $tweet['created_at'] = Carbon::parse($result->created_at);
    return $tweet;
  }
}
