<?php

namespace App\Http\Controllers;

use App\Entities\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
  /**
   * @var UserRepository
   */
  protected $repository;

  /**
   * @var UserValidator
   */
  protected $validator;

  /**
   * UsersController constructor.
   *
   * @param UserRepository $repository
   * @param UserValidator $validator
   */
  public function __construct(UserRepository $repository, UserValidator $validator)
  {
    $this->repository = $repository;
    $this->validator  = $validator;
  }


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
  
    $users = $this->repository->all();
    return response()->json($users);
  }


  /**
   * @return [type]
   */
  public function getCurrentUser()
  {
    $CONSUMER_KEY = config('services.twitter.consumer_key');
    $CONSUMER_SECRET = config('services.twitter.consumer_secret');
    $access_token = config('services.twitter.access_token');
    $access_token_secret = config('services.twitter.access_token_secret');
    $this->connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token, $access_token_secret);
    $currentUser = $this->connection->get("account/settings", []);

    return response()->json($currentUser);
  }
}
