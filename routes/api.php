<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('v1/hi', function () {
  return 'ok!';
});

Route::group([
  'prefix' => 'v1',
  'middleware' => 'throttle:60,1',
  'namespace'  => 'App\Http\Controllers'
], function ($api) {
  $api->get('/tweets', [
      'uses' => 'TweetsController@index',
  ]);

  $api->post('/tweets', [
      'uses' => 'TweetsController@store',
  ]);

  $api->post('/tweets/retweet/{id}', [
      'uses' => 'TweetsController@retweet',
  ]);

  $api->post('/tweets/replyTweet', [
      'uses' => 'TweetsController@replyTweet',
  ]);

  $api->post('/tweets/starTweet', [
      'uses' => 'TweetsController@StarTweet',
  ]);

  $api->get('/users', [
    'uses' => 'UsersController@index',
  ]);

  $api->get('/me/user', [
    'uses' => 'UsersController@getCurrentUser',
  ]);
  
});